function  y  = horners2( d,c,x,b )
%HORNERS2 Summary of this function goes here
%   Detailed explanation goes here

if nargin<4, 
    b=zeros(d,1); 
end
y = c(d+1);
for i=d:-1:1
    y = y.*(x-b(i))+c(i);
end


end

