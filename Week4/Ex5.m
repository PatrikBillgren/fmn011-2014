scrippsy = load('scrippsy.txt')';

x = 1:1:50;



for i = 1:50
    scrippsy(i) = scrippsy(i) - 279;
    y(i) = log(scrippsy(i));
end

plot(x, scrippsy)
A = [ones(1, 50); x]';

r = (A'*A)\(A'*y);

hold on
plot(x, exp(r(1)) * exp(r(2)*x))
plot(x, log(r(1)) + r(2)*x(i), 'black')

rmse1 = 0;
rmse2 = 0;
for i = 1:50
    rmse1 = rmse1 + (scrippsy(i) - exp(r(1)) * exp(r(2)*x(i)))^2;
    rmse2 = rmse2 + (log(scrippsy(i)) - log(r(1)) - r(2)*x(i))^2;
end
rmse1 = sqrt(rmse1/50)
rmse2 = sqrt(rmse2/50)