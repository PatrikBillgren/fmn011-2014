function y = multinewton( f, g, u0, v0 )
%NEWTON Summary of this function goes here
%   Detailed explanation goes here

u = u0;
v = v0;
f
DF = jacobian(f, g);
for i = 1:10
    
    DFe = eval(DF)
    rhs = eval(f).*-1
    s = DFe\rhs

    u = u + s(1)
    v = v + s(2)
end

 
end

