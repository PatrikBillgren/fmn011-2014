function y  = secant( f, x0, x1, iters )
%SECANT Summary of this function goes here
%   Detailed explanation goes here

for i=1:iters
    y(i) = x1 - (f(x1) * (x1 -x0)) / (f(x1) - f(x0));
    x0 = x1;
    x1 = y(i);
end

