
n = 6;

fprintf('Cond: \t\t\t Rel. forward error: \t\t\t Rel. backward error:  \t\t\t EMF :\n'); 
for n=6:15
    for i=1:n
        for k = 1:n
            A(i, k) = 5 /(i + 2 * k - 1);
        end
    end
    x = ones(n, 1);
    
    b = A*x;
    xc = A\b;
    forerr = norm(x - xc, Inf) / norm(x, Inf);
    backerr = norm(b - A*xc, Inf) / norm(b, Inf);
    fprintf('%16e\t\t\t%12e \t\t\t %12e \t\t\t %16e \n', cond(A), forerr, backerr, forerr/backerr);
    res(n-5) = [cond(A)];
end

clear A;
%
%res